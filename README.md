# shutdown-gtk

XFCE logout dialog with focusing on the proper button (Shutdown or Restart).

![screenshot](shutdown-gtk2.webp)

Usage: `shutdown-gtk.py [-h] [-r | -s] [-q] [-p]`

```
Options:
  -h, --help      show this help message and exit
  -r, --reboot    Reboot button will be focused initially
  -s, --shutdown  Shutdown button will be focused initially
  -q, --quiet     Don't print the info message
  -p, --preserve  Save the current session on shutdown
```

Advice: Bind this program to a hotkey, e.g. `<Super>Escape`.

Rationale:
https://forum.xfce.org/viewtopic.php?id=11684

DBus in Python:
https://wiki.python.org/moin/DbusExamples

Tutorial using pydbus:
https://github.com/LEW21/pydbus/blob/master/doc/tutorial.rst

XFCE logout dialog:
https://gitlab.xfce.org/xfce/xfce4-session/-/blob/master/xfce4-session-logout/main.c

XFCE DBus interface:
https://gitlab.xfce.org/xfce/xfce4-session/-/blob/master/xfce4-session/xfsm-manager-dbus.xml

### Minimal example of convenient XFCE logout with DBus

```
#!/usr/bin/env python
import subprocess
from pydbus import SessionBus
bus = SessionBus()
power = bus.get('org.xfce.SessionManager') #, '/org/xfce/SessionManager'
power.Logout(1, '')
subprocess.run(["sleep 0.2; xdotool key Tab key Tab"], shell=True)
```

Main methods:

```
power.Shutdown(allow_save)
power.Restart(allow_save)
power.Logout(show_dialog, allow_save)
```

For example, `power.Logout(1, '')` or `power.Logout(True, False)` for logout with confirmation dialog.

### Installation and requirements

Install packages:

* pygobject https://pygobject.readthedocs.io/en/latest/getting_started.html
* python-pydbus

Download files from this repo.

Run `shutdown-gtk.py` to test it.

In your system settings (Keyboard shortcuts) set a hotkey to start/close this program (e.g. `<Super>Escape`).

### Licenses

Icons:

```
Reboot & Close: Oxygen Team (Oxygen Icons)  LGPL
Shutdown:       Kyo-Tux (Phuzion Icons)     CC-BY-NC-SA 4.0
```