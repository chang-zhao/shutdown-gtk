#!/usr/bin/env python
""" XFCE logout dialog focusing on Shutdown or Reboot buttons

    '-s', '--shutdown'      focus on Shutdown button
    else                    focus on Reboot button
"""
import argparse
import sys
import subprocess
from os import chdir, path
from pydbus import SessionBus
import gi
gi.require_version("Gdk", "3.0")
gi.require_version("Gtk", "3.0")
gi.require_version("Wnck", "3.0")
from gi.repository import Gdk, GdkX11, GLib, Gtk, Wnck

prog_info='Shutdown GTK, v.1.2.'
WM_NAME = "Shutdown or press Escape"
desc=f"{prog_info} XFCE logout focused on Shutdown or Reboot buttons."

############################   Arguments:

parser = argparse.ArgumentParser(
    prog='shutdown-gtk.py',
    description=desc,
    epilog='Advice:\n    Bind this program to a hotkey, e.g. <Super>Escape.')

# By default, focus on "Reboot" (then args.shutdown would be false):

group = parser.add_mutually_exclusive_group()

group.add_argument('-r', '--reboot', action='store_true',
                    help="Reboot button will be focused initially")

group.add_argument('-s', '--shutdown', action='store_true',
                    help="Shutdown button will be focused initially",
                    default=False)

parser.add_argument('-q', '--quiet', action='store_true',
                    help="Don't print the info message",
                    default=False)

parser.add_argument('-p', '--preserve', action='store_true',
                    help="Save the current session on shutdown",
                    default=False)

args = parser.parse_args()

############################   If the program is already open, close it:

screen = Wnck.Screen.get_default()
screen.force_update()
windows = screen.get_windows()

for wn in windows:
    if wn.get_name() == WM_NAME:
        wn.close(GdkX11.x11_get_server_time(Gdk.get_default_root_window()))
        sys.exit()

############################   Init:

if not args.quiet:
    saving_mess = '(Saving the current session).' if args.preserve else\
                '(Not saving the current session).'
    print(prog_info, saving_mess)

loop = GLib.MainLoop()

# Icons (for buttons and the application)

icon_theme = Gtk.IconTheme.get_default()
wIcon = icon_theme.load_icon("system-shutdown", 48, Gtk.IconLookupFlags.USE_BUILTIN)

dir_path = path.dirname(path.realpath(__file__))
chdir(dir_path)
rIcon_name = path.join(dir_path, 'icons/reboot.png')
sIcon_name = path.join(dir_path, 'icons/shutdown.png')
cIcon_name = path.join(dir_path, 'icons/application-exit.png')

############################   Run Shutdown or Reboot:

def shutDown(reboot=True):
    bus = SessionBus()
    power = bus.get('org.xfce.SessionManager')
    if reboot:
        # Saving the session if args.preserve
        power.Restart(args.preserve)
    else:
        power.Shutdown(args.preserve)
    loop.quit()
    sys.exit()

############################   Process key presses:

def onKeyPress(wid, ev):
    """ To quit, press Escape or Ctrl-Q or Ctrl-W (or Alt-F4) """
    k = Gdk.keyval_name(ev.keyval).lower()
    if ev.state & Gdk.ModifierType.CONTROL_MASK:    # Ctrl
        if k == 'q' or k == 'w':
            sys.exit()
            return True
    elif not (ev.state & Gdk.ModifierType.MODIFIER_MASK) and k == 'escape':
            sys.exit()
            return True
    # Event not handled:
    return False

############################   GTK Window:

class MyWindow(Gtk.Window):

    def __init__(self):
        super().__init__(
            title=WM_NAME,
            name="shutdown",
            default_width=972,
            default_height=500,
            border_width=0,
            resizable=False,
            decorated=False,
            icon=wIcon,
            modal=True,
            type_hint=Gdk.WindowTypeHint.DIALOG,
            skip_pager_hint=True,
            skip_taskbar_hint=True,
            window_position=Gtk.WindowPosition.CENTER,
            )
        self.set_border_width(0)

        # Button "Reboot"

        rIcon = Gtk.Image.new_from_file(rIcon_name)
        self.label_r = Gtk.Label(label="Reboot", halign=Gtk.Align.CENTER)
        self.btn_reboot = Gtk.Button(
                                name="rbut",
                                height_request=400,
                                width_request=400)
        self.box_reboot = Gtk.Box(spacing=0, orientation=1)
        self.box_reboot.pack_start(rIcon, True, True, 24)
        self.box_reboot.pack_end(self.label_r, True, True, 24)
        self.btn_reboot.add(self.box_reboot)

        # Button "Shutdown"

        sIcon = Gtk.Image.new_from_file(sIcon_name)
        self.label_s = Gtk.Label(label="Shutdown", halign=Gtk.Align.CENTER)
        self.btn_shutdown = Gtk.Button(
                                name="sbut",
                                height_request=400,
                                width_request=400)
        self.box_shutdown = Gtk.Box(spacing=0, orientation=1)
        self.box_shutdown.pack_start(sIcon, True, True, 24)
        self.box_shutdown.pack_end(self.label_s, True, True, 24)
        self.btn_shutdown.add(self.box_shutdown)

        # Button "Close"

        cIcon = Gtk.Image.new_from_file(cIcon_name)
        self.btn_close = Gtk.Button(
                                name="close",
                                image=cIcon,
                                height_request=120,
                                width_request=120)
        self.box_close = Gtk.Box(spacing=0, orientation=1)
        self.box_close.pack_start(self.btn_close, False, False, 0)

        # Connect & pack buttons

        self.btn_reboot.connect("clicked", self.on_btn_reboot_clicked)
        self.btn_shutdown.connect("clicked", self.on_shutdown_clicked)
        self.btn_close.connect("clicked", lambda x:loop.quit())

        width, height = 972, 500
        self.bar = Gtk.ActionBar(width_request=width,
                                height_request=height,
                                halign=Gtk.Align.CENTER,
                                valign=Gtk.Align.CENTER)
        self.bar.pack_start(self.btn_shutdown)
        self.bar.pack_start(self.btn_reboot)
        self.bar.pack_start(self.box_close)
        self.fullscreen()
        self.add(self.bar)


    def on_btn_reboot_clicked(self, widget):
        shutDown()

    def on_shutdown_clicked(self, widget):
        shutDown(reboot=False)


win = MyWindow()
win.set_visual(win.get_screen().get_rgba_visual())

# CSS:
style_provider = Gtk.CssProvider()
style_provider.load_from_data("""
#shutdown {
  background-color: rgba(120, 150, 180, 0.7);
  border-radius: 18px;
}
actionbar, box, image {
  background-color: transparent;
  border-color: transparent;
  border-radius: 15px;
}
actionbar {
  background-color: rgba(180, 230, 140, 0.6);
  padding: 9px 9px 12px 12px;
}
#sbut, #rbut {
  padding: 0;
  margin: 0px 0px 0px 2px;
  color: #963;
  border-width: 4px;
  border-radius: 15px;
  background: linear-gradient(to bottom, #666 0, #EEE 3%, #E0E0E0 50%, #CCC 96%, #999 99%, #666);
  border-color: rgba(180, 230, 140, 0.4);
}
#sbut:focus, #rbut:focus {
  color: #000;
  background: linear-gradient(to top, #666 0, #FFF 3%, #F0F0F0 60%, #DDD 97%, #EEE 99%, #666);
  border-color: #FF7;
}
#close {
  border-color: rgba(180, 230, 140, 0.05);
  border-radius: 12px;
  border-width: 0;
  padding: 0;
}
#close:focus {
  background-color: #FF7;
  border-color: #FF7;
}
#close image {
  border-radius: 11px;
  background-color: rgb(180, 230, 140);
  padding: 0;
}
#close:focus image {
  background-color: #FF7;
  animation-name: spin;
  animation-duration: 3s;
  animation-timing-function: linear;
  animation-iteration-count: 0.03;
}
label {
  font-size: 72px;
  padding: 0 0 18px 0;
  border-radius: 15px;
}
""")
Gtk.StyleContext.add_provider_for_screen(
    Gdk.Screen.get_default(),
    style_provider,
    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
)

win.show_all()
win.set_keep_above(True)

# Accept keyboard presses:
win.add_events(Gdk.EventMask.KEY_PRESS_MASK)
win.connect("key-press-event", onKeyPress)

win.connect("destroy", lambda x:loop.quit())

# if args.shutdown, focus on "Shutdown". Else on "Reboot"
if args.shutdown:
    focused = win.btn_shutdown
else:
    focused = win.btn_reboot

focused.grab_focus()

loop.run()
sys.exit()
